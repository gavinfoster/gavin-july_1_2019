const documentCardSelector = '[data-test=document-card]'
const documentCardDeleteSelector = '[data-test=document-card-delete]'

describe('Deletes Document', () => {
  it('deletes first document', () => {
    cy.visit('http://localhost:3000')

    cy.get(documentCardSelector).first().find(documentCardDeleteSelector).click()

    cy.get(documentCardSelector).should('have.length', 5)
  })
})