const baseUrl = 'https://private-f0ef4-gavinjuly12019.apiary-mock.com'
const getDocumentsUrl = `${baseUrl}/documents?search=&limit=20&offset=0`
const unreasonableSearchTerm = `supercalifragilisticexpialidocious${Date.now()}{enter}`
const reasonableSearchTerm = 'dog{enter}'
const documentCardSelector = '[data-test=document-card]'
const searchInputSelector = '[data-test=search-input]'
const documentTotalSelector = '[data-test=document-total]'
const filesizeTotalSelector = '[data-test=filesize-total]'
const emptySelector = '[data-test=empty]'

describe('Searches Documents', () => {
  it('successfully loads', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: getDocumentsUrl
    }).as('getDocuments')

    cy.visit('http://localhost:3000')
    cy.wait(['@getDocuments'])

    cy.get(documentCardSelector).should('have.length', 6)
    cy.get(filesizeTotalSelector).should('have.text', '799.26 KB')
    cy.get(documentTotalSelector).should('have.text', '6 documents')
    cy.get(emptySelector).should('not.exist')
  })

  it('searches 0 results', () => {

    cy.get(searchInputSelector).type(unreasonableSearchTerm)
    cy.get(documentCardSelector).should('have.length', 0)
    cy.get(filesizeTotalSelector).should('have.text', '0 B')
    cy.get(documentTotalSelector).should('have.text', '0 documents')
    cy.get(emptySelector).should('exist')
  })

  it('searches 1 results', () => {

    cy.get(searchInputSelector).clear().type(reasonableSearchTerm)
    cy.get(documentCardSelector).should('have.length', 1)
    cy.get(filesizeTotalSelector).should('have.text', '85.94 KB')
    cy.get(documentTotalSelector).should('have.text', '1 documents')
  })
})