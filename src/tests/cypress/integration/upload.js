const uploadButtonSelector = '[data-test=upload-button]'
const documentCardSelector = '[data-test=document-card]'
const uploadModalContentSelector = '[data-test=upload-modal-content]'
const fileSelector = '.filepond--browser'
const fileStatusSelector = '.filepond--file-status-main'
const invalidFileStatus = 'Invalid file type. Please upload a png or jpg'
const fixures = {
  jpeg: {
    path: 'dog.jpeg',
    type: 'image/jpeg',
    name: 'My Little Big Dog Jpeg',
    selector: `[data-name="My Little Big Dog Jpeg"]`
  },
  gif: {
    path: 'dog.gif',
    type: 'image/gif',
    name: 'My Little Big Dog Gif',
    selector: `[data-name="My Little Big Dog Gif"]`
  }
}

describe('Uploads Document', () => {
  it('navigates to upload', () => {
    cy.visit('http://localhost:3000')

    cy.get(uploadButtonSelector).click()
    cy.get(uploadModalContentSelector).should('exist')
    cy.get(fixures.jpeg.selector).should('have.length', 0)
  })
  it('uploads image', () => {
    const { path, name, type } = fixures.jpeg

    cy.get(fileSelector).then(subject => cy.window().then(win => cy
      .fixture(path, 'base64')
      .then(Cypress.Blob.base64StringToBlob)
      .then((blob) => {
        const el = subject[0]
        const testFile = new win.File([blob], name, { type })
        const dataTransfer = new win.DataTransfer()
        dataTransfer.items.add(testFile)
        el.files = dataTransfer.files
        cy.wrap(subject).trigger('change', { force: true })
      })))
      // TODO: removing for MVP: using server's response static name instead of user's filename
      // .then(() => cy.get(selector).should('have.length', 1))
      .then(() => cy.get(documentCardSelector).should('have.length', 7))
  })
  it('navigates back to upload', () => {
    cy.get(uploadModalContentSelector).should('not.exist')
    cy.get(uploadButtonSelector).click()
    cy.get(uploadModalContentSelector).should('exist')
    // TODO: removing for MVP: using server's response static name instead of user's filename
    // cy.get(fixures.gif.selector).should('have.length', 0)
  })
  it('does not upload invalid file type', () => {
    const { path, name, type, selector } = fixures.gif

    cy.get(fileSelector).then(subject => cy.window().then(win => cy
      .fixture(path, 'base64')
      .then(Cypress.Blob.base64StringToBlob)
      .then((blob) => {
        const el = subject[0]
        const testFile = new win.File([blob], name, { type })
        const dataTransfer = new win.DataTransfer()
        dataTransfer.items.add(testFile)
        el.files = dataTransfer.files
        cy.wrap(subject).trigger('change', { force: true })
      })))
      .then(() => cy.get(selector).should('not.exist'))
      .then(() => cy.get(fileStatusSelector).should('exist').should('have.text', invalidFileStatus))
  })
})