import { getDocuments, deleteDocument, createDocument } from '../../helpers/api-wrapper'

describe('API Wrapper', () => {
  test('get documents', async () => {

    const response = await getDocuments()

    expect(response.data).toBeDefined()

    const { limit, offset, total, data } = response.data

    expect(limit).not.toBeNaN()
    expect(offset).not.toBeNaN()
    expect(total).not.toBeNaN()
    expect(Array.isArray(data)).toBe(true);
  })

  test('delete document', async () => {

    const response = await deleteDocument()

    expect(response).toBeDefined()
    expect(response.status).toBe(200)
  })

  test('create document', async () => {

    const response = await createDocument()

    // is GUID
    expect(response.data.length).toBe(36)
  })
})