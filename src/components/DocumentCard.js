import React, { memo, useState } from 'react';
import { Card, Button } from 'antd'
import filesize from 'filesize'
import styled from 'styled-components/macro'
import Loading from './Loading'
import { deleteDocument } from '../helpers/api-wrapper'
import delay from '../helpers/delay'

const DocumentCard = memo(({ id, name, size, onError, onDelete }) => {

  const DELETE_DELAY = 1000

  const Title = styled.h2``

  const Description = styled.div`
    display: flex;
    justify-content: space-between;
  `

  const CardWrapper = styled(Card).attrs(() => ({
    'data-test':'document-card',
    'data-name':name
  }))`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `

  const [isDeleting, setIsDeleting] = useState(false)

  const handleDelete = async (documentId) => {

    try {

      setIsDeleting(true)

      await delay(DELETE_DELAY)

      const response = await deleteDocument(documentId)

      if ([!200, 202, 204].includes(response.status)) {

        throw new Error(`Error deleting document: Status Code ${response.status}`)
      }

      setIsDeleting(false)

      onDelete(documentId)
    } catch (err) {

      onError(err.message)

      setIsDeleting(false)
    }
  }

  return (
    <CardWrapper>
      { isDeleting ? <Loading /> : null }
      <Title>{name}</Title>
      <Description>
        <p>{filesize(size)}</p>
        <Button
          type="danger"
          data-test="document-card-delete"
          onClick={() => handleDelete(id)}>
            Delete
        </Button>
      </Description>
    </CardWrapper>
  )
})

export default DocumentCard;
