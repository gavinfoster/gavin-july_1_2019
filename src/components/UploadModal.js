import React from 'react'
import { Modal } from 'antd'
import Upload from './Upload'
import { DocumentConsumer } from '../DocumentContext'
import delay from '../helpers/delay'

const UploadModal = () => {

  const MODAL_CLOSE_DELAY = 700

  return (
    <DocumentConsumer>
      {({ addDocument, isModalVisible, setIsModalVisible, addError }) => (
        <Modal
          title="Upload Document"
          visible={isModalVisible}
          footer={null}
          destroyOnClose
          onCancel={() => setIsModalVisible(false)}
        >
          <p data-test="upload-modal-content">Upload either a jpg or png, with a maximum upload size of 10MB</p>
          <Upload
            onError={addError}
            onComplete={async (file) => {
              addDocument(file)
              await delay(MODAL_CLOSE_DELAY)
              setIsModalVisible(false)
            }}
          />
        </Modal>
      )}
    </DocumentConsumer>
  )
}

export default UploadModal