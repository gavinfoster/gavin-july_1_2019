import React, { useState } from 'react'
import { FilePond, registerPlugin } from 'react-filepond'
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size'
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type'
import 'filepond/dist/filepond.min.css'

registerPlugin(FilePondPluginFileValidateSize, FilePondPluginFileValidateType)

const Upload = ({ onComplete, onError }) => {

  const [files, setFiles] = useState([])

  const handleProcessedFile = (response) => {
    try {

      const parsedResponse = JSON.parse(response)

      onComplete(parsedResponse)

    } catch(err) {

      onError(err.message)
    }
  }

  return (
    <FilePond
      name="image-upload[file]"
      files={files}
      maxFileSize='10MB'
      server={{
        url: `${process.env.REACT_APP_API_BASE_URL}/documents`,
        process: {
          onload: handleProcessedFile
        }
      }}
      allowRevert={false}
      acceptedFileTypes={['image/png', 'image/jpeg', 'image/jpg']}
      labelMaxFileSizeExceeded="File is too large. Please upload a file less than 10MB"
      labelFileTypeNotAllowed="Invalid file type. Please upload a png or jpg"
      onupdatefiles={fileItems => setFiles(fileItems.map(fileItem => fileItem.file))}
    />
  )
}

export default Upload