import React from 'react';
import styled from 'styled-components/macro'
import filesize from 'filesize'
import { DocumentConsumer } from '../DocumentContext'

const SubHeaderWrapper = styled.header`
  width: 100%;
  display: flex;
  justify-content: space-between;
`

const SubHeaderTitle = styled.h3``

const Lowercase = styled.span`
  text-transform: lowercase;
`

const SubHeader = () => (
  <DocumentConsumer>
    {({ isLoading, documents }) => {

      if (isLoading) return null

      const totalFileSize = documents.map(({ size }) => size).reduce((a,b) => a + b, 0)

      return (
        <SubHeaderWrapper>
          <SubHeaderTitle data-test="document-total">{documents.length} documents</SubHeaderTitle>
          <p>Total size: <Lowercase data-test="filesize-total">{filesize(totalFileSize)}</Lowercase></p>
        </SubHeaderWrapper>
      )
    }}
  </DocumentConsumer>
)

export default SubHeader;