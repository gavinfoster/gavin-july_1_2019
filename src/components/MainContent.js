import React from 'react';
import styled from 'styled-components/macro'
import isEmpty from 'lodash.isempty'
import Empty from './Empty'
import Loading from './Loading'
import DocumentCard from './DocumentCard'
import { DocumentConsumer } from '../DocumentContext'

const Main = styled.main`
  position: relative;
  min-height: 300px;
`

const Grid = styled.section`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: ${({ theme: { marginSmall } }) => marginSmall};

  ${({ theme: { medium }}) => medium`
    grid-template-columns: repeat(3, 1fr);
  `};
`

const MainContent = () => (
  <DocumentConsumer>
    {({ documents, isLoading, removeDocument, addError }) => (
      <Main>{
        (isLoading)
        ? <Loading />
        :(isEmpty(documents))
        ? <Empty />
        : <Grid>{ documents.map(file => (
            <DocumentCard
              onError={addError}
              onDelete={removeDocument}
              key={file.id}
              {...file}
            />))}
          </Grid>
      }</Main>
    )}
  </DocumentConsumer>
)

export default MainContent;
