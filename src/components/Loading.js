import React from 'react'
import styled from 'styled-components'
import { Icon } from 'antd'

const IconWrapper = styled(Icon)`
  z-index: 20;
  position:absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
  font-size:3rem;
  background-color: rgba(255, 255, 255, .9)
`

const LoadingIcon = () => (
  <IconWrapper data-test="loading" type="loading" />
)

export default LoadingIcon