import React, { memo } from 'react'
import styled from 'styled-components'
import { Empty } from 'antd'

const EmptyWrapper = styled(Empty)`
  width: 100%;
  height: 300px;
  display:flex;
  flex-direction:column;
  align-items: center;
  justify-content: center;
`

const EmptyComponent = memo(() => (
  <EmptyWrapper
    image={Empty.PRESENTED_IMAGE_SIMPLE}
    description="No documents uploaded"
    data-test="empty" />
))

export default EmptyComponent