import { createContext } from 'react'

const DocumentContext = createContext({})

export const DocumentProvider = DocumentContext.Provider
export const DocumentConsumer = DocumentContext.Consumer
export default DocumentContext
