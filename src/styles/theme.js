import { css } from 'styled-components/macro'

const mediaQueryGenerator = (...queryFeatures) => (...rules) => css`
@media ${css(...queryFeatures)} {
  ${css(...rules)}
}`

const theme = {
  marginSmall: `.5em`,
  maginMedium: `.75em`,
  medium: mediaQueryGenerator`(min-width: 768px)`,
  print: mediaQueryGenerator`print`,
}

export default theme