import axios from 'axios'

export const getDocuments = async (search = ``, limit = 20, offset = 0) => {

  const response = await axios.get(`${process.env.REACT_APP_API_BASE_URL}/documents`, {
    params: {
      search,
      limit,
      offset
    }
  })

  // Replicate query string filtering (isn't supported by apiary)
  const responseWithFilteredData = Object.assign({}, response, {
    data: Object.assign({}, response.data, {
      data: response.data.data.filter(({ name }) => name.toLowerCase().includes(search.toLowerCase()))
    })
  })

  return responseWithFilteredData
}

export const deleteDocument = (documentId) => axios.delete(`${process.env.REACT_APP_API_BASE_URL}/documents/${documentId}`)

export const createDocument = () => axios.post(`${process.env.REACT_APP_API_BASE_URL}/documents`)