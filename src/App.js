import React, { Component } from 'react';
import { Input, Button, notification } from 'antd'
import styled, { ThemeProvider } from 'styled-components/macro'
import { DocumentProvider } from './DocumentContext'
import MainContent from './components/MainContent'
import SubHeader from './components/SubHeader'
import UploadModal from './components/UploadModal'
import styleTheme from './styles/theme'
import { getDocuments } from './helpers/api-wrapper'
import delay from './helpers/delay'

const { Search } = Input;

const Main = styled.main`
  max-width: 960px;
  margin: 0 auto;
  padding: 2em .5em .5em;
  position:relative;
`

const Header = styled.header`
  width: 100%;
  display: flex;
  flex-direction:column;
  justify-content: space-between;
  padding-bottom: 2em;

  ${({ theme: { medium }}) => medium`
    flex-direction:row;
  `};
`

const SearchInput = styled(Search)`
  width: 100%;
  order: 1;

  ${({ theme: { medium }}) => medium`
    width: 50%;
    order: 0;
  `};
`

const ButtonInput = styled(Button)`
  text-transform: uppercase;
  width: 100%;
  margin-bottom: ${({ theme: { maginMedium } }) => maginMedium};

  ${({ theme: { medium }}) => medium`
    width: auto;
    order: 1;
    margin-bottom: unset;
  `};
`

class App extends Component {

  state = {
    documents: [],
    isLoading: false,
    isModalVisible: false,
    setIsModalVisible: isModalVisible => this.setState(() => ({ isModalVisible })),
    setIsLoading: isLoading => this.setState(() => ({ isLoading })),
    setDocuments: documents => this.setState(() => ({ documents })),
    addDocument: document => this.setState(state => ({ documents: [...state.documents, document]})),
    removeDocument: documentId => this.setState(state => ({ documents: state.documents.filter(({ id }) => id !== documentId)})),
    addError: err => notification.error({ message: `Error`, description: err.message})
  }

  componentDidMount () {
    this.fetchData()
  }

  fetchData = async (search = '') => {
    const { setIsLoading, setDocuments, addError } = this.state

    try {
      setIsLoading(true)

      await delay(1000)

      const response = await getDocuments(search)

      const { data } = response.data

      setDocuments(data)
    } catch (err) {
      addError(err)
    }
    setIsLoading(false)
  }

  render () {
    const { setIsModalVisible } = this.state
    return (
      <DocumentProvider value={this.state}>
        <ThemeProvider theme={styleTheme}>
          <Main>
            <Header>
              <SearchInput
                allowClear
                enterButton
                placeholder="Search documents..."
                data-test="search-input"
                onSearch={this.fetchData} />
              <ButtonInput
                type="primary"
                icon="download"
                data-test="upload-button"
                onClick={() => setIsModalVisible(true)}>
                  Upload
              </ButtonInput>
            </Header>
            <SubHeader />
            <MainContent />
            <UploadModal />
          </Main>
        </ThemeProvider>
      </DocumentProvider>
    )
  }
}

export default App;
