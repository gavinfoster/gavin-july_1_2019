# Gavin - July 1, 2019

[Live Version](https://goofy-bohr-c62780.netlify.com/)

## Installation

Clone repo and run `npm install` in order to install dependencies

### `npm start`

Runs the app in the development mode<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Tests

#### `npm test`

Runs a series of unit, functional, and snapshot jest tests<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run cypress`

Opens the [cypress](https://www.cypress.io/) electron test runner where you can click the .js files to run individual tests or "Run All Tests". `npm run cypress-headless` will run the tests headlessly and you will see the output in your terminal.

![](http://g.recordit.co/HulYX4RoUf.gif)

## Security

Addressed Concerns:

- limit file size, and upload file type
- npm audit / locking versions

**Not** Addressed Concerns:

- Rate limiting
- Scan for malware (ClamAV)
- Code obfuscation on build (JScrambler)
- Remove unecessary metadata like exif, limit filename size
- Randomize / escape filename
- Sanitize inputs like search etc.
- CORS headers
- `--ignore-scripts` for installed npm packages
- Content-Security-Policy: ssl / restrict to asset's cdn
- SSL / Strict-Transport-Security header
- Repository dependency scanning

## Improvements

- **Accessibility**

  Better semantic html (heading structure), navigation, aria attributes (describedby etc.) More complete input component (form and label elements). Inline errors associated with input. More useful error messages for all types of errors.

- **Abstraction of API call / set state / error pattern**

  Created a simple api-wrapper, but could look at breaking out repeated pattern into a helper and use axios interceptor in api wrapper for handling errors.

- **Modal Usage**

  It seems unecessary for the upload button to trigger a modal, but I think given the real estate in the mockup and the desire for additional contextual information it is necessary. The automatically closing modal also isn't great, but I think a cleaner solution than some sort of cancel / upload modal buttons if there aren't any additional inputs.

- **Styles**

  [Styled Components](https://www.styled-components.com/docs/basics#motivation) are nice, but I think similar to the `mediaQueryGenerator()` helper function, some re-use patterns could emerge from repeating css (common combinations of: flex, align / justify center, height / width 100% etc). Depending on the team and design patterns, using a utility class library like [tailwind](https://tailwindcss.com) could be nice and easily work with styled-components.

  More fluid breakpoints could be nice especially for the grid around a tablet size.

- **Error Handling**

  Error handling could definitely be improved. Right now it just uses the default antd notification, which was just used for ease of implementation. Ideally the errors would be more context specific and provide better information.

- **Adding Routing**

  Separate routes for the upload modal or a delete confirmation. Add react-helmet for any document head related changes.

- **State Management**

  In this case it might not be really necessary to use the Context API and instead use useEffect for API calls on mount and useState otherwise. The components here don't get nested that deeply, but it just seemed easier to anticipate the growth or change of scope.

- **Component Design**

  I decided to use antd since the design patterns were simple, but could imagine that as this application grew it'd be useful to develop a pattern library (even if it was just antd as a base) and spinning up a storybook environment. Loading component would be a good first candidate.

  Similarly, The components in App.js should really just setup the Providers to wrap the app and provide some very basic layout structure. Components like <SubHeader /> should just take some layout css and get children to render in order to decouple components like the filesize calculator or document count. The problem was just that App.js was getting too unweildy and large, especially for a code exercise.

  **Components**

  Add prop type validation and defaults.

- **[International language support](https://github.com/formatjs/react-intl)**

- **Progressive Web App**

  Good considerations summed up with [Create React App](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

- **Typescript**

  I've yet to really dive into typescript, so I didn't get caught up on trying to use it for this. Feels like even creating simple interfaces for the documents would be beneficial.

- **Serverside Rendering**

  Could have used Next.js as a framework to get started with SSR.

- **Test Coverage**

  Cypress is a good start for e2e testing. Currently some basic search / delete / upload flows. Jest is used for basic component tests and snapshots along with functional tests for the api-wrapper. These could certainly be refined and expanded: e.g. more tests for errors etc.

- **Additional functionality**

  Search: Users have gotten used to search including suggestions, previously search, type ahead etc. Expanding the API to include createdOn and allowing users to search or sort would be beneficial.

  Pagination: This has been suggested by the api-wrapper implementation, but some form of pagination is necessary.

  Upload: Support multiple files, rename etc.

## Libraries

- [React Create App](https://facebook.github.io/create-react-app/docs/getting-started)

  I'm most familiar with RCA as a framework and it's very popular.

- [Styled Components](https://www.styled-components.com/)

  Incredibly legible and easy to use. [Additional Benefits](https://www.styled-components.com/docs/basics#motivation)

- [Antd](https://ant.design/)

  Good breadth of components and solid documentation. Very popular.

- Eslint and Prettify

  Airbnb linting rules. This is much different than what I use at my current job, but it's an industry standard and much less idiosyncratic than how things are written at Knack.

- [Axios](https://github.com/axios/axios)

  Client / Server HTTP client that's really popular. As compared to fetch it automatically transforms json.

- [Filesize.js](https://github.com/avoidwork/filesize.js)

  Convert bytes to human readable format easily

- [Filepond](https://pqina.nl/filepond/docs/patterns/frameworks/react/)

  Using filepond react and some plugins for validation. It comes with nice styles out of the box and is pretty easy to use.

- Lodash

  Using `lodash.isempty` to explicitly check if documents are empty. As handles undefined and null values.

- `react-test-renderer` & Jest

  Jest is already a part of react create app, but `react-test-renderer` is an easy way to create snap shot tests of components.

## API

[Apiary Documentation](https://gavinjuly12019.docs.apiary.io)

I used Apiary since it was a very easy way to mock out an api and automatically generates some documentation. I'm using the data returned from the responses, which you can when adding files etc. I'm not really familiar with the template generation so forgive the somewhat wonky documentation.

Limit and offset are referenced in the api documentation along with the API wrapper, but aren't implemented beyond that. Also, since I'm using Apiary which sends back static JSON defined in the markdown, uploading a file will return back a JSON response that doesn't reflect the uploaded file.

The resource is named `documents` due to the language in the mockups, even though in reality these are just images.

### GET /documents/?search=&limit=&offset=

Request: Accepts optional query string parameters:

- search
- limit
- offset

Response: 200 Status Code. Returns JSON with document data and pagination information

```
{
  "limit": 20,
  "offset": 0,
  "total": 6,
  "data": [
    {
      "id": "be2031b4-d1e1-418b-aa14-37754b0bb5c8",
      "name": "Misty-Fog",
      "url": "https://s3.amazonaws.com/public-upload-service/documents/misty-fog.jpg",
      "size": 98000
    }
  ]
}
```

### POST /documents

Request: `Content-Type:multipart/form-data; boundary=---BOUNDARY`. Only accepts png / jpg files under 10MB.

Response: 201 Status Code. JSON with created document

```
{
  "id": "be2031b4-d1e1-418b-aa14-37754b0bb999",
  "name": "Bio-pic-final",
  "url": "https://s3.amazonaws.com/public-upload-service/documents/Bio-pic-final.png",
  "size": 123040
}
```

### DELETE /documents/:documentId

Request: Required documentId parameter

Response: 200 Status Code

## Additional Notes

I've added some delays in order to better emulate a real app experience.

A lot of people hate nested ternarys and you can see the usage of one in MainContent.js but I'd refer to this [article](https://medium.com/javascript-scene/nested-ternaries-are-great-361bddd0f340).
